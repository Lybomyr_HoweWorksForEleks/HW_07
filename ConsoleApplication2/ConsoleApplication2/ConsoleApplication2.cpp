#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <fstream>

using namespace std;

class Contact
{
public:
	string name;
	string phone;
	string email;

public:
	Contact(string Name, string Phone, string Email)
	{
		name = Name;
		phone = Phone;
		email = Email;
	}

	bool operator == (Contact contact)
	{
		return name == contact.name;
	}

	string ToString()
	{
		return  
			"Name is : " + name + "\n" +
			"Phone number - " + phone + "\n" + 
			"Email - " + email + "\n";
	}
};

class Organizer
{
public:	vector<Contact> contacts;

	void AddingNewContactToList(Contact contact)
	{
		contacts.push_back(contact);
	}

public:

	void CreateNewContact()
	{
		string name;
		string phone;
		string email;

		cout << "Please enter user name \n";
		cin >> name;
		cout << "Please enter phone \n";
		cin >> phone;
		cout << "Enter email \n";
		cin >> email;

		Contact newContact(name, phone, email);

		AddingNewContactToList(newContact);

		cout << "New contact was added successfully!\n";
	}

	void ShowAllContacts()
	{
		for (int i = 0; i < contacts.size(); i++)
		{
			cout << contacts[i].ToString();
		}
	}

	void ShowContactByName(string name)
	{
		for (int i = 0; i < contacts.size(); i++)
		{
			if (contacts[i].name == name)
			{
				cout << contacts[i].ToString();
			}
		}
	}

	void DeleteContact(string name)
	{
		for (int i = 0; i < contacts.size(); i++)
		{
			if (contacts[i].name == name)
			{
				contacts.erase(contacts.begin() + i);
			}
		}
		 
		cout << "Contact was deleted successfully! \n";
	}
};

class File
{
public: string path;

public:
	void WritingInFile(vector<Contact> contactsForWriting)
	{
		ofstream file;
		if (path != "")
		{
			file.open(path);
		}
		else
		{
			file.open("1.txt");
		}

		for (int i = 0; i < contactsForWriting.size(); i++)
		{
			file << contactsForWriting[i].ToString();
		}
		
		file.close();
	}


};

class UInterface
{
public:
    void Menu()
	{
		cout << "1 - add new contact \n2 - show all contacts \n3 - show contact by name \n4 - delete selected contact";
		cout << "\n5 - Load from file \n6 - Save in file \n0 - exit and close program!\n";
	}

    void SwitchingAction()
	{
		int key;
		Organizer myOrganaizer;
		bool flag = false;
		do
		{
			cin >> key;

			switch (key)
			{
			case 0:
			{
				flag = true;
			}
			break;

			case 1:
			{
				myOrganaizer.CreateNewContact();
			}
			break;

			case 2:
			{
				myOrganaizer.ShowAllContacts();
			}
			break;

			case 3:
			{
				string name;
				cout << "Please enter name \n";
				cin >> name;
				myOrganaizer.ShowContactByName(name);
			}
			break;

			case 4:
			{
				string name;
				cout << "Please enter name for delete contact \n";
				cin >> name;
				myOrganaizer.DeleteContact(name);
			}
			break;

			case 5:
			{
				//load from
			}
			break;

			case 6:
			{
				File file;
				file.WritingInFile(myOrganaizer.contacts);
			}
			break;

			default:
			{
				cout << "Please enter correct value!\n";
			}
			break;
			}

			Menu();

		} while (!flag);

	}
};



int main()
{
	UInterface UI;
	UI.Menu();
	UI.SwitchingAction();
	
	
	system("pause");
	return 0;
	
}